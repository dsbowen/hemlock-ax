import numpy as np
from hemlock import User, Page, create_app, socketio
from hemlock.questions import Input, Label
from hemlock_ax import init_app, init_test_app, assign_user, make_assigner
from sqlalchemy_mutable.utils import partial

assigner = make_assigner({"factor0": (0, 1), "factor1": (0, 1)})

@User.route("/survey")
def seed():
    assignment = assign_user(assigner)
    return [
        Page(
            Input(
                make_advertisement(**assignment),
                input_tag={"type": "number", "min": 0},
                # by default, the model assumes you're trying to optimize a variable
                # named "target"
                variable="target",
                test_response=partial(make_test_response, **assignment)
            )
        ),
        Page(
            Label("Thank you for your participation!")
        ),
    ]

def make_advertisement(factor0, factor1):
    return f"""
        Advertisement {factor0} {factor1}.

        How much would you pay for this product?
    """

def make_test_response(question, factor0, factor1):
    return 0.1 * factor0 + 0.2 * factor1 + np.random.normal()


app = create_app()

if __name__ == "__main__":
    init_test_app(app)  # use test initialization for development environment
    socketio.run(app, debug=True)
else:
    init_app(app)  # use regular initialization for production environment
