import numpy as np
import pandas as pd
import pytest
from hemlock_ax import Assigner
from hemlock_ax.models import linear_regression


@pytest.mark.parametrize(
    "conditions", ({"factor0": (0, 1, 2)}, {"factor0": (0, 1), "factor1": (0, 1)})
)
@pytest.mark.parametrize("with_control", (False, True))
def test_linear_regression(
    conditions, with_control, n_per_condition=10, endog_name="target"
):
    # create the assigner
    control = None
    if with_control:
        control = tuple([values[0] for values in conditions.values()])
    assigner = Assigner(conditions, control=control)

    # simulate an experiment and estimate the model
    records = []
    for items in assigner.possible_assignments:
        records.append({key: value for key, value in zip(assigner.factor_names, items)})
    df = pd.DataFrame(n_per_condition * records)
    df[endog_name] = df.sum(axis=1) + np.random.normal(scale=1e-5)
    df["id"] = df.index
    possible_assignments, dist = linear_regression(
        df, assigner.factor_names, control=assigner.control, endog_name=endog_name
    )

    # test expected return values
    expected_assignments = set(assigner.possible_assignments)
    if with_control:
        expected_assignments.remove(assigner.control)
    assert set(possible_assignments) == expected_assignments

    expected_means = np.array([sum(assignment) for assignment in possible_assignments])
    if with_control:
        expected_means -= sum(assigner.control)
    np.testing.assert_allclose(dist.mean, expected_means, atol=1e-2)
