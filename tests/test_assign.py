import numpy as np
import pandas as pd
import pytest
from hemlock import User, Page
from scipy.stats import multivariate_normal
from sqlalchemy_mutable.utils import partial

from hemlock_ax import Assigner, init_test_app

from .utils import test_app as app, assigner, conditions


def seed(assigner):
    assigner.assign_user()
    return Page(data=[("target", 10 * np.random.normal())])


def test_model_kwargs(conditions):
    assert Assigner(conditions).model_kwargs == {"exog_names": list(conditions.keys())}


@pytest.mark.parametrize("n_users", (1, 2, 3))
@pytest.mark.parametrize("pr_run", (0, 1))
def test_get_jobs(app, assigner, n_users, pr_run):
    init_test_app(app, pr_run=pr_run)
    User.test_multiple_users(
        n_users, partial(seed, assigner), test_kwargs={"verbosity": 0}
    )

    n_in_progress = len(assigner.get_jobs("enqueued"))
    n_finished = len(assigner.get_jobs("finished"))
    if pr_run == 0:
        assert n_in_progress == 1
        assert n_finished == 0
    else:
        assert n_in_progress == 0
        assert n_finished == (1 if n_users == 1 else 2)


def test_refresh(app, assigner):
    User.test_multiple_users(
        4 * len(assigner.possible_assignments),
        partial(seed, assigner),
        test_kwargs={"verbosity": 0},
    )
    job = assigner.get_jobs("finished")[-1]
    weights, pr_best = job.result[1:]
    assigner.refresh()
    assert job in assigner.get_jobs("finished")
    assert assigner.weights == weights
    assert assigner.pr_best == pr_best


def test_job_lifecycle(app, assigner):
    pr_run_key = "PR_AX_JOB_RUN"

    # job is enqueued but hasn't run
    app.config[pr_run_key] = 0
    assigner.refresh(enqueue_new_job=True)
    job = assigner.get_jobs("enqueued")[0]
    assert job in app.ax_queue.jobs
    assert job.id not in app.ax_queue.finished_job_registry.get_job_ids()

    # job has run, no new job enqueued
    app.config[pr_run_key] = 1
    assigner.refresh()
    assert job not in app.ax_queue.jobs
    assert job.id in app.ax_queue.finished_job_registry.get_job_ids()

    # job has been deleted
    # the first time a new job is enqueued, the original job isn't deleted but is no longer the latest job to finished
    # the job is deleted when the second job is enqueued
    assigner.refresh(enqueue_new_job=True)
    assigner.refresh(enqueue_new_job=True)
    assert job.delete.called
    assert job.id not in app.ax_queue.finished_job_registry.get_job_ids()


def test_assign_user(app, assigner):
    n_conditions = len(assigner.possible_assignments)

    with pytest.warns(RuntimeWarning, match="Assignment weights not yet set*"):
        User.make_test_user(partial(seed, assigner)).test(verbosity=0)

    for _ in range(10):
        job = assigner.get_jobs("finished")[-1]
        # assign user to 0th condition with probability 1
        weights = [1] + (n_conditions - 1) * [0]
        job.result = (
            assigner.possible_assignments,
            weights,
            np.full(n_conditions, 1 / n_conditions).tolist(),
        )
        user = User.make_test_user(partial(seed, assigner))
        user.test(verbosity=0)
        assignment = tuple(user.meta_data[key] for key in assigner.factor_names)
        assert assignment == assigner.possible_assignments[0]


def test_no_completed_users(app, assigner):
    # test that the assigner behaves like the random assigner before weights are set
    expected_assignments = set(assigner.possible_assignments)
    for _ in range(len(expected_assignments)):
        assignment = assigner.assign_user(User.make_test_user())
        expected_assignments.remove(
            tuple(assignment[name] for name in assigner.factor_names)
        )
    assert not expected_assignments


def test_some_completed_users(app, assigner):
    # test that the assigner assigns users only to conditions with no completed
    # users after some users start completing the study
    # but still not enough to fit the model
    assignment = assigner.assign_user(user := User.make_test_user())
    user.meta_data["target"] = 0
    user.completed = True
    assignments = []
    for _ in range(2 * len(assigner.possible_assignments)):
        assignments.append(assigner.assign_user(User.make_test_user()))
    assert assignment not in assignments


@pytest.mark.parametrize("min_users", (0, 2, 4))
def test_min_users_per_condition(app, min_users):
    # test that the adaptive assigner doesn't kick in until the minimum number of users
    # are in each condition
    i, max_iter = 0, 100
    assigner = Assigner(
        {"treat": (0, 1)},
        model=pandas_model,
        min_users_per_condition=min_users,
        allow_singular=True,
    )
    while not assigner.weights:
        assert i < max_iter
        i += 1
        assigner.assign_user(user := User.make_test_user())
        user.meta_data["target"] = 0
        user.completed = True

    assert (assigner.get_cum_assigned()["count"] >= min_users).all()


def pandas_model(df, **kwargs):
    # behaves as if returning a dataframe with columns equal to possible assignments
    # and data as a matrix of samples
    return pd.DataFrame({(0,): [0, 0], (1,): [1, 1]})


def dist_model(df, **kwargs):
    # behaves as if returning (list of possible assignments, distribution)
    return [(0,), (1,)], multivariate_normal([0, 1], [[1e-5, 0], [0, 1e-5]])


def matrix_model(df, **kwargs):
    # behaves as if returning (list of possible assignments, matrix of samples)
    return [(0,), (1,)], np.array([[0, 1], [0, 1]])


def failed_model(df, **kwargs):
    # tests behavior when the model fails to fit
    # model fit is expected to fail early in the study when there's not enough data
    raise ValueError("Some error")


def extra_assignments_model(df, **kwargs):
    # tests behavior when the model returns extra possible assignments
    return [(1,), (2,)], np.array([[0, 1], [0, 1]])


def missing_assignments_model(df, **kwargs):
    # tests behavior when the model returns missing possible assignments
    # missing possible assignments are expected early in the study when there's enough
    # data to estimate the effects of some but not all conditions
    return [(0,)], np.array([0, 0])


def singular_cov_matrix_model(df, **kwargs):
    # tests behavior when the estimated covariance matrix is singular
    return [(0,), (1,)], multivariate_normal(
        [0, 0], [[0, 0], [0, 1]], allow_singular=True
    )


@pytest.mark.parametrize("acquisition", ("thompson", "exploration"))
@pytest.mark.parametrize(
    "model",
    (
        pandas_model,
        dist_model,
        matrix_model,
        failed_model,
        extra_assignments_model,
        missing_assignments_model,
        singular_cov_matrix_model,
    ),
)
@pytest.mark.filterwarnings("ignore")
def test__update_assignment_weights(app, model, acquisition):
    assigner = Assigner({"factor0": (0, 1)}, model=model, acquisition=acquisition)
    if model != singular_cov_matrix_model:
        assigner.allow_singular = True

    if model == extra_assignments_model:
        with pytest.raises(ValueError):
            assigner._update_assignment_weights(app.config)
        return

    if model == missing_assignments_model:
        with pytest.warns(RuntimeWarning):
            result = assigner._update_assignment_weights(app.config)
        assert result is None
        return

    if model == singular_cov_matrix_model:
        with pytest.warns(RuntimeWarning):
            result = assigner._update_assignment_weights(app.config)
        # should put all the weight on the condition where the standard deviation is 0
        assert result[1] == [1, 0]
        return

    result = assigner._update_assignment_weights(app.config)
    if model == failed_model:
        assert result is None
    else:
        possible_assignments, weights, pr_best = result

        assert possible_assignments == [(0,), (1,)]
        assert pr_best == [0, 1]
        if acquisition == "thompson":
            assert weights == [0, 1]
        else:
            assert weights == [0.5, 0.5]


@pytest.mark.filterwarnings("ignore")
def test__update_assignment_weights_with_control(app):
    assigner = Assigner(
        {"treatment": ("control", 0, 1)},
        control="control",
        model=pandas_model,
        acquisition="thompson",
        allow_singular=True,
    )
    possible_assignments, weights, pr_best = assigner._update_assignment_weights(
        app.config
    )
    # control should have None as the probability of being best and the maximum weight
    index = possible_assignments.index(assigner.control)
    assert pr_best[index] is None
    assert weights[index] == max(weights)
