import pytest
from hemlock import User, create_test_app
from hemlock.app import db

from hemlock_ax import Assigner, init_app, init_test_app
from hemlock_ax.assign import assigners


@pytest.fixture
def test_app():
    app = create_test_app()
    init_test_app(app)
    yield app
    for user in User.query.all():
        db.session.delete(user)
    assigners.clear()


@pytest.fixture
def app():
    app = create_test_app()
    init_app(app)
    yield app
    for user in User.query.all():
        db.session.delete(user)
    assigners.clear()


@pytest.fixture(params=({"factor0": (0, 1)}, {"factor0": (0, 1), "factor1": (0, 1)}))
def conditions(request):
    return request.param


@pytest.fixture
def assigner(conditions):
    assigners.clear()
    return Assigner(conditions)
