import os

import pytest
from hemlock import User, Page

from hemlock_ax._admin_route import static_pages
from hemlock_ax.assign import assigners

from .utils import test_app as app, assigner, conditions


@pytest.mark.filterwarnings("ignore")
@pytest.mark.parametrize("in_gitpod", (True, False))
@pytest.mark.parametrize("with_users", (True, False))
def test_admin_ax(app, assigner, in_gitpod, with_users):
    def seed():
        assigner.assign_user()
        return Page(data=[("target", 0)])

    # clear the global assigners variable
    # this is used to create the ax label
    assigners.clear()
    assigners.append(assigner)

    if in_gitpod:
        os.environ["GITPOD_HOST"] = "host"

    if "ax" in static_pages:
        static_pages.pop("ax")

    if with_users:
        User.make_test_user(seed).test(verbosity=0)

    with app.test_client() as client:
        response = client.get("/admin-ax")

    if in_gitpod:
        os.environ.pop("GITPOD_HOST")

    assert response.status == "200 OK"
    assert b"Weights" in response.data
    assert b"Cum. Assigned" in response.data
    assert (b"Proportion" in response.data) == with_users
