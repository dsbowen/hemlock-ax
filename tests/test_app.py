from rq import Queue

from .utils import app


class TestInitApp:
    def test_redis_url(self, app):
        assert app.config["REDIS_URL"] == "redis://"

    def test_ax_queue(self, app):
        assert isinstance(app.ax_queue, Queue)
