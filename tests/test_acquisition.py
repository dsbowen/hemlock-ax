import numpy as np

from hemlock_ax.acquisition import exploration, thompson


dist = np.array([[0, 1], [0, 1], [0, 1], [1, 0]])


def test_exploration():
    assert (exploration(dist) == [0.25 * 0.75, 0.75 * 0.25]).all()


def test_thompson():
    assert (thompson(dist) == [0.25, 0.75]).all()
