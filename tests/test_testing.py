import numpy as np
import pytest

from hemlock import Page

from hemlock_ax import run_test

from .utils import app, test_app, assigner, conditions


@pytest.mark.filterwarnings("ignore")
def test_run_test(test_app, assigner):
    # Note: this is a quick test to make sure testing.test can run without error
    # It is difficult to verify the correctness of the output due to stochasticity
    # TODO: make this test more robust
    def seed():
        assigner.assign_user()
        return Page(data=[("target", np.random.normal())])

    result = run_test(16, seed)
    assert len(result) == 1
    # result[0] is the summary dataframe for the zeroeth assigner
    assert len(result[0]) > 0
