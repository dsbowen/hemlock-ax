.. Hemlock Ax documentation master file, created by
   sphinx-quickstart on Mon Nov 12 14:17:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hemlock Ax documentation
===========================================

A `Hemlock <https://dsbowen.gitlab.io/hemlock>`_ extension for adaptive experimentation.

.. image:: https://gitlab.com/dsbowen/hemlock-ax/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/hemlock-ax/-/commits/master
.. image:: https://gitlab.com/dsbowen/hemlock-ax/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/hemlock-ax/-/commits/master
.. image:: https://badge.fury.io/py/hemlock-ax.svg
   :target: https://badge.fury.io/py/hemlock-ax
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fhemlock-ax/HEAD?urlpath=lab
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

..
   .. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
      :target: https://gitlab.com/dsbowen/hemlock-ax/-/blob/master/LICENSE

|
Start here
==========

.. raw:: html

    <iframe width="640" height="360" src="https://www.youtube.com/embed/TsUjT-BY_BY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
Get started with `Hemlock <https://dsbowen.gitlab.io/hemlock>`_.

Create a repository based on the `Hemlock Ax template <https://github.com/dsbowen/hemlock-ax-template/generate>`_.

API reference
=============

.. toctree::
   :maxdepth: 2

   hemlock_ax/index
   Changelog <changelog>

..
   .. autosummary::
      :toctree: hemlock_ax

      hemlock_ax.acquisition
      hemlock_ax.app
      hemlock_ax.assign
      hemlock_ax.models
      hemlock_ax.testing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
=========

.. code-block::

   
   @software(bowen2021hemlock-ax,
      title={ Hemlock Ax },
      author={ Bowen, Dillon },
      year={ 2021 },
      url={ https://dsbowen.gitlab.io/hemlock-ax }
   )

License
=======

Hemlock Ax currently has a closed-source license requiring written permission from its original author, Dillon Bowen, for use and distribution. I intend for Hemlock Ax to be an open-research software: free for academics and non-profit use but not for commercial purposes. The closed-source license is temporary until I have an appropriate open-research license. Until then, please feel free to use hemlock for academic and non-profit purposes and for personal use.