﻿hemlock\_ax.assign
==================

.. automodule:: hemlock_ax.assign

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_data
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Assigner
   
   

   
   
   



