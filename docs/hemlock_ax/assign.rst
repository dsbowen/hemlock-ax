hemlock\_ax.assign
==================

.. automodule:: hemlock_ax.assign
   :members:
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_data
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Assigner
   
   

   
   
   



