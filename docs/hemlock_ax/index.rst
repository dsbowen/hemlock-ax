API reference
=============

.. toctree::
    :maxdepth: 2

    Acquisition functions <acquisition>
    App <app>
    Assign <assign>
    Models <models>
    Testing <testing>