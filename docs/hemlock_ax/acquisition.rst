hemlock\_ax.acquisition
=======================

.. automodule:: hemlock_ax.acquisition
   :members:
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      exploration
      thompson
   
   

   
   
   

   
   
   



