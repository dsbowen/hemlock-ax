# Hemlock Ax

[![pipeline status](https://gitlab.com/dsbowen/hemlock-ax/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/hemlock-ax/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/hemlock-ax/badges/master/coverage.svg)](https://gitlab.com/dsbowen/hemlock-ax/-/commits/master)
[![PyPI version](https://badge.fury.io/py/hemlock-ax.svg)](https://badge.fury.io/py/hemlock-ax)
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/hemlock-ax/-/blob/master/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fhemlock-ax/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

A hemock extension for adaptive experimentation. [Read the docs](https://dsbowen.gitlab.io/hemlock-ax).
