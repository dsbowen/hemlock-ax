"""Acquisition functions.
"""
from __future__ import annotations

from typing import Callable, Dict

import numpy as np


def get_control_weight(weights: np.ndarray) -> float:
    """Get the assignment weight for the control arm.

    Args:
        weights (np.ndarray): (# treatment arms,) array of assignment weights.

    Returns:
        float: Weight on the control arm.
    """
    return weights.max()


def thompson(samples: np.ndarray) -> np.ndarray:
    """Thompson sampling.

    Args:
        samples (np.ndarray): (n sample, n arms) array of samples from a distribution
            of effects.

    Returns:
        np.ndarray: (n arms,) array of assignment weights.
    """
    return np.identity(samples.shape[1])[samples.argmax(axis=1)].mean(axis=0)


def exploration(samples: np.ndarray) -> np.ndarray:
    """Exploration sampling.

    Args:
        samples (np.ndarray): (n sample, n arms) array of samples from a distribution
            of effects.

    Returns:
        np.ndarray: (n arms,) array of assignment weights.
    """
    weights = thompson(samples)
    return weights * (1 - weights)


functions: Dict[str, Callable[[np.ndarray], np.ndarray]] = {
    "exploration": exploration,
    "thompson": thompson,
}
