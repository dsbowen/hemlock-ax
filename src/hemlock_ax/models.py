"""Models.
"""
from __future__ import annotations

from typing import Any, Callable, List, Protocol, Tuple, Union

import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.stats import multivariate_normal


class DataFrame(Protocol):
    columns: list[Any]
    values: np.ndarray


class Distribution(Protocol):
    def rvs(self, size: int, *args: Any, **kwargs: Any) -> np.ndarray:
        ...  # pragma: no cover


ModelReturnType = Union[DataFrame, Tuple[List[Tuple], Union[np.ndarray, Distribution]]]


def linear_regression(
    df: pd.DataFrame,
    exog_names: list[str],
    control: tuple = None,
    endog_name: str = "target",
) -> ModelReturnType:
    """Estimate the distribution using linear regression.

    Args:
        df (pd.DataFrame): Data used to fit the model.
        exog_names (list[str]): Names of exogenous (assignment) variables.
        control (tuple, optional): Values of the control arm.
        endog_name (str, optional): Name of the endogenous variable. Defaults to
            "target".

    Returns:
        ModelReturnType: list of possible assignments, distribution of effects.
    """
    df = df.dropna(subset=exog_names + [endog_name])
    X = pd.get_dummies(df[exog_names].apply(lambda row: tuple(row), axis=1))
    if control is not None:
        X[control] = 1
    model = sm.OLS(df[endog_name], X)
    results = model.fit().get_robustcov_results("cluster", groups=df.id)
    possible_assignments = model.exog_names
    if control is None:
        mean, cov = results.params, results.cov_params()
    else:
        mask = np.array([(col != control) for col in model.exog_names])
        possible_assignments.remove(control)
        mean, cov = results.params[mask], results.cov_params()[mask][:, mask]
    return possible_assignments, multivariate_normal(mean, cov, allow_singular=True)


models: dict[
    str,
    Callable[..., ModelReturnType],
] = {"linear_regression": linear_regression}
